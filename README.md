# distances

A python package for calculating various distance measures

# Installation

```shell
git clone https://gitlab.gwdg.de/c/Users/AMDAMATAC/Desktop/distances.git

# Usage

Currently the following distance measures are implemented in the package:
- Euclidean distance.

# Tests

The package is being tested using `pytest`. To run the test suite use the
command:
```shell
$ pytest test_euclidean
```
# License
The package is under the MIT license.